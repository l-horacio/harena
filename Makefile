CS=main.c X11Window.c world.c font.c htree.c map_loader.c entity.c
CC=gcc
CFLAGS=-Wall -Wextra -Werror -pedantic -Wuninitialized -Wshadow -ggdb
LDFLAGS=$$(pkg-config --libs --cflags freetype2) -lX11 -lGL

.PHONY: all
all: harena

harena: main.c X11Window.c
	$(CC) $(CFLAGS) $(LDFLAGS) $(CS) -o harena

harena-release: main.c
	$(CC) -O3 $(CS) -o harena-release

clear:
	rm harena
