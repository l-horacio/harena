#include "window.h"
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysymdef.h>

#include <GL/gl.h>
#include <GL/glx.h>

#include <locale.h>
#include <stdlib.h>
#include <uchar.h>
#include <unistd.h>
#include <string.h>
#include <wchar.h>

struct window {
    Display *display;
    Window window;
    Atom deleteWin;
    XIC xic;
    event_callback event_handler;
};

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display *, GLXFBConfig, GLXContext, Bool, const int*) ;

static XIMStyle choose_better_style(XIMStyle style1, XIMStyle style2);
static int translate_keycode(int keycode, unsigned char block);

struct window *create_window(int width, int height, event_callback callback)
{
    int screen_id = -1;
    static struct window main_win = {0};

    XInitThreads();
    setlocale(LC_ALL, "");

    main_win.display = XOpenDisplay(NULL);
    if (!main_win.display) {
        exit(1);
    }

    screen_id = DefaultScreen(main_win.display);

    GLint glx_attrs[] = {
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_RGBA,
        GLX_DOUBLEBUFFER,
        GLX_RED_SIZE, 4,
        GLX_GREEN_SIZE, 4,
        GLX_BLUE_SIZE, 4,
        GLX_DEPTH_SIZE, 16,
        None
    };

    XVisualInfo *visual = glXChooseVisual(main_win.display, screen_id, glx_attrs);

    GLint major_GLX, minor_GLX = 0;
    glXQueryVersion(main_win.display, &major_GLX, &minor_GLX);

    XSetWindowAttributes windowAttrs;
    windowAttrs.border_pixel = BlackPixel(main_win.display, screen_id);
    windowAttrs.background_pixel = WhitePixel(main_win.display, screen_id);
    windowAttrs.override_redirect = True;
    windowAttrs.colormap = XCreateColormap(main_win.display, RootWindow(main_win.display, screen_id), visual->visual, AllocNone);
    windowAttrs.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask;


    main_win.window = XCreateWindow
        (
            main_win.display,
            RootWindow(main_win.display, screen_id),
            0, 0,
            width, height,
            0,
            visual->depth,
            InputOutput,
            visual->visual,
            CWBackPixel | CWColormap | CWBorderPixel | CWEventMask,
            &windowAttrs);

    Atom atom_wm_delete_window = XInternAtom(main_win.display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(main_win.display, main_win.window, &atom_wm_delete_window, 1);
    main_win.deleteWin = atom_wm_delete_window;

    GLXContext context = glXCreateContext(main_win.display, visual, 0, True);

    XSync(main_win.display, False);

    glXMakeCurrent(main_win.display, main_win.window, context);

    XIM xim = XOpenIM(main_win.display, 0, 0, 0);

    XIMStyle app_xim_styles = XIMPreeditNone | XIMPreeditNothing | XIMPreeditArea | XIMStatusNone | XIMStatusNothing | XIMStatusArea;

    XIMStyles *supported_styles = 0;
    XGetIMValues(xim, XNQueryInputStyle, &supported_styles, NULL);

    XIMStyle best_style = 0;
    for (int i = 0; i < supported_styles->count_styles; ++i) {
        XIMStyle style = supported_styles->supported_styles[i];
        if((style & app_xim_styles) == style)
            best_style = choose_better_style(style, best_style);
    }
    XFree(supported_styles);

    main_win.xic = XCreateIC(xim,  XNInputStyle, best_style, XNClientWindow, main_win.window, NULL);
    long im_event_mask = 0;
    XGetICValues(main_win.xic, XNFilterEvents, &im_event_mask, NULL);
    XSetICFocus(main_win.xic);

    long event_mask =
        ExposureMask |
        KeyPressMask |
        KeyReleaseMask |
        StructureNotifyMask |
        ButtonPressMask |
        ButtonReleaseMask |
        PointerMotionMask |
        FocusChangeMask |
        im_event_mask;

    XSelectInput(main_win.display, main_win.window, event_mask);

    glClearColor(0.f, 0.f, 0.f, 1.0f);
    glViewport(0, 0, 800, 600);

    XClearWindow(main_win.display, main_win.window);
    XMapRaised(main_win.display, main_win.window);

    main_win.event_handler = callback;

    return &main_win;
}

static XIMStyle choose_better_style(XIMStyle style1, XIMStyle style2)
{
    XIMStyle s,t;
    XIMStyle preedit = XIMPreeditArea | XIMPreeditCallbacks | XIMPreeditPosition | XIMPreeditNothing | XIMPreeditNone;
    XIMStyle status = XIMStatusArea | XIMStatusCallbacks | XIMStatusNothing | XIMStatusNone;
    if (style1 == 0) return style2;
    if (style2 == 0) return style1;
    if ((style1 & (preedit | status)) == (style2 & (preedit | status))) return style1;
    s = style1 & preedit;
    t = style2 & preedit;
    if (s != t)
    {
        if (s | t | XIMPreeditCallbacks)
            return (s == XIMPreeditCallbacks)?style1:style2;
        else if (s | t | XIMPreeditPosition)
            return (s == XIMPreeditPosition)?style1:style2;
        else if (s | t | XIMPreeditArea)
            return (s == XIMPreeditArea)?style1:style2;
        else if (s | t | XIMPreeditNothing)
            return (s == XIMPreeditNothing)?style1:style2;
    }
    else
    {
        // if preedit flags are the same, compare status flags
        s = style1 & status;
        t = style2 & status;
        if (s | t | XIMStatusCallbacks)
            return (s == XIMStatusCallbacks)?style1:style2;
        else if (s | t | XIMStatusArea)
            return (s == XIMStatusArea)?style1:style2;
        else if (s | t | XIMStatusNothing)
            return (s == XIMStatusNothing)?style1:style2;
    }

    return 0;
}

int update_window(struct window *win, struct state *state)
{
    XEvent ev;
    while(XPending(win->display)) {
        XNextEvent(win->display, &ev);
        switch (ev.type) {
        case Expose:
        {
            XWindowAttributes attrs;
            XGetWindowAttributes(win->display, win->window, &attrs);
            if (win->event_handler)
                win->event_handler(win,
                                   state,
                                   (struct event){WE_RESIZE,
                                                  .window_size = {attrs.width, attrs.height}});
            break;
        }
        case ClientMessage:
            if(ev.xclient.data.l[0] == (long) win->deleteWin){
                return 1;
            }
            break;
        case DestroyNotify:
            return 1;
        case KeyPress:
        case KeyRelease:
        {
            if(!win->event_handler)
                return 0;

            unsigned char pressed = ev.type == KeyPress;
            if(!pressed && XPending(win->display))
            {
                XEvent next;
                XPeekEvent(win->display, &next);

                if(next.type == KeyPress && next.xkey.keycode == ev.xkey.keycode && next.xkey.time == ev.xkey.time)
                    continue;
            }

            unsigned code = 0;
            {
                int kc = ev.xkey.keycode;
                if(kc >= 0 && kc <= 0xFF) {
                    code = translate_keycode(kc, 0);
                } else {
                    int count = 0;
                    KeySym sym = 0, *mapped = XGetKeyboardMapping(win->display, kc, 1, &count);
                    if (count > 0 && !mapped && mapped[0] != NoSymbol) {
                        sym = mapped[0];
                    }
                    XFree(mapped);

                    if(sym != 0) {
                        if(sym >> 8 == 0xFF) {
                            code = translate_keycode(kc, 1);
                        } else if (sym >> 8  == 0 && sym == 32) {
                            code = WE_SPACE;
                        }
                    }
                }
            }

            if (!code)
                continue;

            wchar_t buf[255] = {0};
            KeySym key;
            Status status;
            int rc = XwcLookupString(win->xic, &ev.xkey, buf, 255, &key, &status);
            if(rc <= 0 || (code >= WE_RETURN && code < WE_SPACE) || code >= WE_F1 || buf[0] < 32) {
                buf[0] = 0;
            }

            win->event_handler(win,
                               state,
                               (struct event){WE_KEYPRESS,
                                              .keypress_info = {(code | (pressed ? 0 : WE_RELEASE)), buf[0]}});
            break;
        }
        case MotionNotify:
            if(!win->event_handler)
                return 0;

            win->event_handler(win,
                               state,
                               (struct event){WE_MOUSE_MOVE,
                                              .mouse_position = {ev.xmotion.x, ev.xmotion.y}});
            break;
        case ButtonPress:
        case ButtonRelease:
        {
            if(!win->event_handler)
                return 0;

            unsigned char pressed = ev.type == ButtonPress;
            unsigned char is_button = (ev.xbutton.button >= 1 && ev.xbutton.button <= 7);

            if(is_button) {
                static int mapping[] = {
                    0,
                    WE_MOUSE_LEFT,
                    WE_MOUSE_MIDDLE,
                    WE_MOUSE_RIGHT,
                    WE_MOUSE_WHEEL_UP,
                    WE_MOUSE_WHEEL_DOWN,
                    WE_MOUSE_X1,
                    WE_MOUSE_X2
                };

                int code = mapping[ev.xbutton.button];
                if(code)
                    win->event_handler(win,
                                       state,
                                       (struct event) {WE_MOUSE,
                                                       .mouse_button = code | (pressed ? 0 : WE_RELEASE)});
            } else {
                continue;
            }
            break;
        }
        }
    }
    return 0;
}

static int translate_keycode(int kc, unsigned char block)
{
    if(block){
        switch (kc) {
        case XK_BackSpace&0xFF: return WE_BACKSPACE;
        case XK_Tab&0xFF: return WE_TAB;
        case XK_Return&0xFF: return WE_RETURN;
        case XK_Pause&0xFF: return WE_PAUSE;
        case XK_Escape&0xFF: return WE_ESCAPE;
        case XK_Delete&0xFF: return WE_DELETE;
        case XK_KP_0&0xFF: return WE_KP_0;
        case XK_KP_1&0xFF: return WE_KP_1;
        case XK_KP_2&0xFF: return WE_KP_2;
        case XK_KP_3&0xFF: return WE_KP_3;
        case XK_KP_4&0xFF: return WE_KP_4;
        case XK_KP_5&0xFF: return WE_KP_5;
        case XK_KP_6&0xFF: return WE_KP_6;
        case XK_KP_7&0xFF: return WE_KP_7;
        case XK_KP_8&0xFF: return WE_KP_8;
        case XK_KP_9&0xFF: return WE_KP_9;
        case XK_KP_Insert&0xFF: return WE_KP_0;
        case XK_KP_End&0xFF: return WE_KP_1;
        case XK_KP_Down&0xFF: return WE_KP_2;
        case XK_KP_Page_Down&0xFF: return WE_KP_3;
        case XK_KP_Left&0xFF: return WE_KP_4;
        case XK_KP_Begin&0xFF: return WE_KP_5;
        case XK_KP_Right&0xFF: return WE_KP_6;
        case XK_KP_Home&0xFF: return WE_KP_7;
        case XK_KP_Up&0xFF: return WE_KP_8;
        case XK_KP_Page_Up&0xFF: return WE_KP_9;
        case XK_KP_Delete&0xFF: return WE_KP_PERIOD;
        case XK_KP_Decimal&0xFF: return WE_KP_PERIOD;
        case XK_KP_Divide&0xFF: return WE_KP_DIVIDE;
        case XK_KP_Multiply&0xFF: return WE_KP_MULTIPLY;
        case XK_KP_Subtract&0xFF: return WE_KP_MINUS;
        case XK_KP_Add&0xFF: return WE_KP_PLUS;
        case XK_KP_Enter&0xFF: return WE_KP_ENTER;
        case XK_KP_Equal&0xFF: return WE_EQUALS;
        case XK_Up&0xFF: return WE_UP;
        case XK_Down&0xFF: return WE_DOWN;
        case XK_Right&0xFF: return WE_RIGHT;
        case XK_Left&0xFF: return WE_LEFT;
        case XK_Insert&0xFF: return WE_INSERT;
        case XK_Home&0xFF: return WE_HOME;
        case XK_End&0xFF: return WE_END;
        case XK_Page_Up&0xFF: return WE_PAGEUP;
        case XK_Page_Down&0xFF: return WE_PAGEDOWN;
        case XK_F1&0xFF: return WE_F1;
        case XK_F2&0xFF: return WE_F2;
        case XK_F3&0xFF: return WE_F3;
        case XK_F4&0xFF: return WE_F4;
        case XK_F5&0xFF: return WE_F5;
        case XK_F6&0xFF: return WE_F6;
        case XK_F7&0xFF: return WE_F7;
        case XK_F8&0xFF: return WE_F8;
        case XK_F9&0xFF: return WE_F9;
        case XK_F10&0xFF: return WE_F10;
        case XK_F11&0xFF: return WE_F11;
        case XK_F12&0xFF: return WE_F12;
        case XK_Shift_R&0xFF: return WE_SHIFT;
        case XK_Shift_L&0xFF: return WE_SHIFT;
        case XK_Control_R&0xFF: return WE_CONTROL;
        case XK_Control_L&0xFF: return WE_CONTROL;
        case XK_Alt_R&0xFF: return WE_ALT;
        case XK_Alt_L&0xFF: return WE_ALT;
        default: return kc;
        }
    } else {
        switch (kc) {
        case 0x0A: return WE_1;
        case 0x0B: return WE_2;
        case 0x0C: return WE_3;
        case 0x0D: return WE_4;
        case 0x0E: return WE_5;
        case 0x0F: return WE_6;
        case 0x10: return WE_7;
        case 0x11: return WE_8;
        case 0x12: return WE_9;
        case 0x13: return WE_0;
        case 0x14: return WE_MINUS;
        case 0x15: return WE_EQUALS;
        case 0x18: return WE_Q;
        case 0x19: return WE_W;
        case 0x1A: return WE_E;
        case 0x1B: return WE_R;
        case 0x1C: return WE_T;
        case 0x1D: return WE_Y;
        case 0x1E: return WE_U;
        case 0x1F: return WE_I;
        case 0x20: return WE_O;
        case 0x21: return WE_P;
        case 0x22: return WE_LBRACKET;
        case 0x23: return WE_RBRACKET;
        case 0x26: return WE_A;
        case 0x27: return WE_S;
        case 0x28: return WE_D;
        case 0x29: return WE_F;
        case 0x2A: return WE_G;
        case 0x2B: return WE_H;
        case 0x2C: return WE_J;
        case 0x2D: return WE_K;
        case 0x2E: return WE_L;
        case 0x2F: return WE_SEMICOLON;
        case 0x30: return WE_APOSTROPHE;
        case 0x31: return WE_GRAVE;
        case 0x33: return WE_BACKSLASH;
        case 0x34: return WE_Z;
        case 0x35: return WE_X;
        case 0x36: return WE_C;
        case 0x37: return WE_V;
        case 0x38: return WE_B;
        case 0x39: return WE_N;
        case 0x3A: return WE_M;
        case 0x3B: return WE_COMMA;
        case 0x3C: return WE_PERIOD;
        case 0x3D: return WE_SLASH;
        case 0x5B: return WE_KP_PERIOD;
        case 0x5E: return WE_BACKSLASH;
        case 0x77: return WE_DELETE;
        default: return 0;
        }
    }
}

void window_swap_buffers(struct window *win)
{
    glXSwapBuffers(win->display, win->window);
}
