#include "entity.h"
#include <GL/gl.h>
#include <GL/glx.h>
#include "world.h"

unsigned hash6432shift(unsigned long key)
{
  key = (~key) + (key << 18);
  key ^= (key >> 31);
  key = (key + (key << 2)) + (key << 4);
  key ^= (key >> 11);
  key += (key << 6);
  key ^= (key >> 22);
  return (unsigned) key;
}

unsigned terrain_rand(int x, int y)
{
    return hash6432shift(((unsigned long)x << 32) | ((unsigned long)y & 0xffffffffL));
}

char32_t entity_glyph(struct entity *entity)
{
    switch (entity->type) {
    case ET_PLAYER:
        return '@';
    case ET_ENEMY:
        return 'G';
    case ET_NPC:
        return 'N';
    case ET_TERRAIN:
    {
        const char32_t *grass_tiles = U",.`';´¸";
        int grass_tiles_size = 7;
        int i = terrain_rand(entity->pos.x, entity->pos.y) % grass_tiles_size;
        return grass_tiles[i];
    }
    case ET_TREE:
        return U'◉';
    case ET_WATER:
        return  U'░';
    }
    return 0;
}

void create_entity(struct world *world, int x, int y, enum entity_type type, struct color color)
{
    struct entity *entity = world->state.entities + world->state.entity_count++;
    entity->type = type;
    entity->color = color;
    entity->pos = (struct ipoint) {x, y};
    char32_t g = entity_glyph(entity);
    entity->glyph = load_font_char(world, g);
}

struct entity *map_entity_at(struct map *map, int x, int y)
{
    int i = y * map->size.width + x;
    return map->map_entities + i;
}

void entity_move(struct entity *entity, struct map *map, int dx, int dy)
{
    int new_x =  clamp(entity->pos.x + dx, 0, map->size.width - 1);
    int new_y = clamp(entity->pos.y + dy, 0, map->size.height - 1);
    struct entity *map_entity = map_entity_at(map, new_x, new_y);
    if (has_flag(map_entity->flags, EF_UNPASSABLE))
        return;

    entity->pos.x = new_x;
    entity->pos.y = new_y;
}

void entity_render(struct entity *entity,  int x, int y)
{
    x += entity->glyph->bearing.x;
    y -= (entity->glyph->useful_space.h - entity->glyph->bearing.y);
    int w = entity->glyph->useful_space.w;
    int h = entity->glyph->useful_space.h;
    if(entity->color.a > 0 && entity->glyph->texture) {
        glEnd();
        glBindTexture(GL_TEXTURE_2D, entity->glyph->texture);
        glBegin(GL_QUADS);

        glColor4ub(entity->color.r, entity->color.g, entity->color.b, entity->color.a);

        glTexCoord2f(entity->glyph->texture_coords.tu1, entity->glyph->texture_coords.tv1);
        glVertex2i(x, y);

        glTexCoord2f(entity->glyph->texture_coords.tu1, entity->glyph->texture_coords.tv2);
        glVertex2i(x, y+h);

        glTexCoord2f(entity->glyph->texture_coords.tu2, entity->glyph->texture_coords.tv2);
        glVertex2i(x+w, y+h);

        glTexCoord2f(entity->glyph->texture_coords.tu2, entity->glyph->texture_coords.tv1);
        glVertex2i(x+w, y);
    }
}
