#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <uchar.h>
#include "size.h"

struct color {
    unsigned char r, g, b, a;
};

#define COLOR_BLACK (struct color) {0, 0, 0, 255}
#define COLOR_BLUE (struct color) {0, 45, 168, 255}
#define COLOR_GREEN (struct color) {12, 140, 0, 255}
#define COLOR_CYAN (struct color) {0, 135, 123, 255}
#define COLOR_RED (struct color) {155, 8, 0, 255}
#define COLOR_MAGENTA (struct color) {156, 10, 144, 255}
#define COLOR_BROWN (struct color) {153, 106, 13, 255}
#define COLOR_LIGHT_GRAY (struct color) {190, 174, 171, 255}
#define COLOR_DARK_GRAY (struct color) {128, 117, 114, 255}
#define COLOR_LIGHT_BLUE (struct color) {43, 98, 255, 255}
#define COLOR_LIGHT_GREEN (struct color) {43, 255, 43, 255}
#define COLOR_LIGHT_CYAN (struct color) {43, 255, 255, 255}
#define COLOR_LIGHT_RED (struct color) {255, 13, 34, 255}
#define COLOR_LIGHT_MAGENTA (struct color) {242, 51, 210, 255}
#define COLOR_YELLOW (struct color) {243, 240, 39, 255}
#define COLOR_WHITE (struct color) {243, 240, 231, 255}

enum entity_type {
    ET_TERRAIN,
    ET_TREE,
    ET_WATER,
    ET_PLAYER,
    ET_ENEMY,
    ET_NPC
};

enum entity_flags {
    EF_UNPASSABLE = 1,
    EF_VISIBLE
};

#define has_flag(x, flag) ((x) & (flag)) == (flag)

struct entity {
    enum entity_type type;
    struct font_char *glyph;
    struct ipoint pos;
    struct color color;
    enum entity_flags flags;
//    union{};
};

struct world;
struct map;

void create_entity(struct world *world, int x, int y, enum entity_type type,
                   struct color color);

char32_t entity_glyph(struct entity *entity);
void entity_move(struct entity *entity, struct map *map, int dx, int dy);
void entity_render(struct entity *entity,  int x, int y);
#endif //_ENTITY_H_
