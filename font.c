#include "font.h"
#include "size.h"
#include <ft2build.h>
#include <stdlib.h>
#include <uchar.h>
#include <unistd.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include FT_FREETYPE_H


struct font_provider {
    FT_Face face;
    FT_Library ft;
};

struct font_provider *load_fonts(void) {
    const char *font_path = "./KreativeSquare.ttf";
    static struct font_provider p;
    FT_Init_FreeType(&p.ft);
    FT_New_Face(p.ft, font_path, 0, &p.face);
    return &p;
}

void clear_fonts(struct font_provider *provider) {
    FT_Done_Face(provider->face);
    FT_Done_FreeType(provider->ft);
}

void set_font_size(struct font_provider *provider, int size)
{
    FT_Set_Pixel_Sizes(provider->face, 0, size);
}

unsigned int next_p2(unsigned int a)
{
        unsigned int rval=1;
        // rval<<=1 is a prettier way of writing rval*=2;
        while(rval<a) rval<<=1;
        return rval;
}

struct tex_coords calc_text_coords(struct irectangle region)
{
    float x1 = region.x;
    float x2 = region.x + region.w;
    float y1 = region.y;
    float y2 = region.y + region.h;

    return (struct tex_coords)
        {
            x1 / region.w,
            y1 / region.h,
            x2 / region.w,
            y2 / region.h
        };
}

void load_char(struct font_provider *provider, char32_t glyph, struct font_char* out)
{
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    if(FT_Load_Char(provider->face, glyph, FT_LOAD_RENDER))
        return;

    FT_GlyphSlot slot = provider->face->glyph;

    int rows = slot->bitmap.rows;
    int columns = 0;
    int pixel_size = 0;

    switch (slot->bitmap.pixel_mode) {
    case FT_PIXEL_MODE_GRAY:
        columns = slot->bitmap.width;
        pixel_size = 1;
        break;
    case FT_PIXEL_MODE_LCD:
        columns = slot->bitmap.width/3;
        pixel_size = 3;
        break;
    case FT_PIXEL_MODE_MONO:
        columns = slot->bitmap.width;
        pixel_size = 0;
        break;
    }

    GLubyte expanded_data[columns * rows * 4];

    for (int y = 0; y < rows; y++) {
        for (int x = 0; x < columns; x++) {
            int idx = (y * columns + x) * 4;
            uint8_t* p = slot->bitmap.buffer + y * slot->bitmap.pitch + x * pixel_size;
            switch (slot->bitmap.pixel_mode) {
            case FT_PIXEL_MODE_GRAY:
            {
                expanded_data[idx] = 255;
                expanded_data[idx + 1] = 255;
                expanded_data[idx + 2] = 255;
                expanded_data[idx + 3] = p[0];
                break;
            }
            case FT_PIXEL_MODE_LCD:
            {
                expanded_data[idx] = p[0];
                expanded_data[idx + 1] = p[1];
                expanded_data[idx + 2] = p[2];
                expanded_data[idx + 3] = 255;
                break;
            }
            case FT_PIXEL_MODE_MONO:
            {
                int j = x%8;
                int i = (x-j)/8;
                uint8_t byte = *(slot->bitmap.buffer + y*slot->bitmap.pitch + i);
                uint8_t alpha = (byte & (1 << (7-j)))? 255: 0;
                expanded_data[i] = alpha;
                expanded_data[i + 1] = 255;
                expanded_data[i + 2] = 255;
                expanded_data[i + 3] = 255;
                break;
            }
            }
        }
    }

    glGenTextures(1, &(out->texture));
    glBindTexture(GL_TEXTURE_2D, out->texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, columns, rows,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, expanded_data);

    out->useful_space = (struct irectangle) {0, 0, columns, rows};
    out->texture_coords = calc_text_coords(out->useful_space);
    out->bearing = (struct ipoint) {slot->bitmap_left, slot->bitmap_top};
    out->advance = slot->advance.x >> 6;
    out->glyph = glyph;
}
