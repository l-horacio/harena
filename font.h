#ifndef _FONT_H_
#define _FONT_H_
#include "size.h"
#include <uchar.h>

struct font_provider;

struct font_char {
    unsigned int texture;
    char32_t glyph;
    struct irectangle useful_space;
    struct tex_coords texture_coords;
    struct ipoint offset;
    struct ipoint bearing;
    unsigned advance;
};

struct font_provider *load_fonts(void);
void set_font_size(struct font_provider*, int);
void load_char(struct font_provider*, char32_t, struct font_char*);
void clear_fonts(struct font_provider *);
#endif //_FONT_H_
