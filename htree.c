#include "htree.h"
#include "font.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uchar.h>

// htreetps://attractivechaos.wordpress.com/2009/09/29/khash-h/
#define TABLE_SIZE 32

//htreetp://www.cse.yorku.ca/~oz/hash.htreeml
unsigned long hash(char32_t key, unsigned long t_size)
{
    size_t hash = 5381;
    hash = (hash << 5) + hash + key;
    return hash % t_size;
}

struct htree *htree_make(void)
{
    struct htree *htree = (struct htree *)malloc(sizeof(struct htree));
    htree->elems = calloc(TABLE_SIZE, sizeof(struct htree_elem));
    htree->size = TABLE_SIZE;
    return htree;
}

void increase_htree_size(struct htree *htree)
{
    unsigned long old_size = htree->size;
    unsigned long new_size = old_size * 2;
    struct htree_elem *old_elems = htree->elems;

    htree->elems = calloc(new_size, sizeof(struct htree_elem));
    htree->size = new_size;

    for (unsigned long  i = 0; i < old_size; ++i) {
        if(old_elems[i].val.texture){
            htree_add(htree, old_elems[i].key, old_elems[i].val);
        }
    }

    free(old_elems);
}

void htree_free(struct htree *htree)
{
    for (size_t i = 0; i < TABLE_SIZE; ++i) {
        free(htree->elems + i);
    }
    free(htree->elems);
    free(htree);
}

void htree_add(struct htree *htree, char32_t key, struct font_char val)
{
    unsigned long slot = hash(key, htree->size);
    struct htree_elem elem = htree->elems[slot];

    if (elem.val.texture == 0) {
        htree->elems[slot] = (struct htree_elem) {key, val};
        return;
    }

    // WARNING: Dangerous recoursive stuff
    increase_htree_size(htree);
    htree_add(htree, key, val);
}

struct font_char *htree_get(struct htree *htree, char32_t key)
{
    unsigned long slot = hash(key, htree->size);

    struct htree_elem *elem = htree->elems + slot;

    if (elem->val.texture) {
        return &(elem->val);
    }

    return 0;
}


void print_htree(struct htree *htree, unsigned char print_empty)
{
    printf("===========HTREE BEGIN[%lu]===========\n", htree->size);
    for (unsigned long i = 0; i < htree->size; ++i) {
        struct htree_elem *elem = htree->elems + i;
        if (print_empty || elem->val.texture) {
            printf("htree[%lu]: %u - %u\n", i, elem->val.glyph, elem->val.texture);
        }
    }
    printf("===========HTREE END===========\n\n\n");
}
