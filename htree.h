#ifndef _HTREE_H_
#define _HTREE_H_

#include "font.h"
#include <stddef.h>

struct htree_elem {
    char32_t key;
    struct font_char val;
};

struct htree {
    struct htree_elem *elems;
    unsigned long size;
};

struct htree *htree_make(void);
void htree_free(struct htree *htree);
void htree_add(struct htree *htree, char32_t key, struct font_char val);
struct font_char *htree_get(struct htree *htree, char32_t key);

void print_htree(struct htree *htree, unsigned char print_empty);
#endif //_HTREE_H_
