#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <uchar.h>
#include "size.h"
#include "window.h"
#include "world.h"
#include "font.h"

int main(void)
{
    struct world world = create_world(64, 64);

    world.win = create_window(800, 600, event_handler);
    load_glyphs(&world);

    create_map(&world, "./map1.ppm");

    create_entity(&world, 3, 3, ET_PLAYER, COLOR_WHITE);
    create_entity(&world, 10, 3, ET_ENEMY, COLOR_RED);
    create_entity(&world, 3, 10, ET_NPC, COLOR_YELLOW);
    while(1){
        if(run_loop(&world))
            break;
    }
    return 0;
}
