#include "map_loader.h"
#include "entity.h"
#include "htree.h"
#include "size.h"
#include <stdio.h>
#include <stdlib.h>
#include <uchar.h>

unsigned long measure_until(char *buf, char stop)
{
    unsigned long i = 0;
    while(buf[i] != '\0' && buf[i] != stop)
        ++i;
    return i;
}

struct map load_map(const char *filepath, struct htree *tile_map)
{
    struct map map = (struct map){(struct isize){0, 0}, 0};
    FILE *fp = fopen(filepath, "r");
    fseek(fp, 0, SEEK_END);
    unsigned long f_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char *file_buf = malloc(f_size);
    fread(file_buf, 1, f_size, fp);
    char *file_view = file_buf;

    if(!(file_view[0] == 'P' && file_view[1] == '6'))
        goto end;

    file_view += 3;

    if (file_view[0] == '#')
        file_view += measure_until(file_view, '\n') + 1;

    int w = atoi(file_view);
    if(w < 1)
        goto end;
    map.size.width = w;
    file_view += measure_until(file_view, ' ');

    int h = atoi(file_view);
    if(h < 1)
        goto end;
    map.size.height = h;

    file_view += measure_until(file_view, '\n') + 1;
    file_view += measure_until(file_view, '\n') + 1;
    map.map_entities = malloc(w * h * sizeof(struct entity));
    unsigned long i = 0;
    while (file_view < (file_buf + f_size)) {
        unsigned char r = file_view[0];
        unsigned char g = file_view[1];
        unsigned char b = file_view[2];
        int x = i % w;
        int y = i / w;
        struct entity *entity = map.map_entities + i;
        entity->pos.x = x;
        entity->pos.y = y;
        if (g == 0 && r == 0 && b == 255) { // WATER
            entity->color = COLOR_BLUE;
            entity->type = ET_WATER;
            entity->flags = EF_UNPASSABLE;
        } else if (r == 255 && b == 255 && g == 0) { // TREE
            entity->color = COLOR_BROWN;
            entity->type = ET_TREE;
            entity->flags = EF_UNPASSABLE;
        } else { // DEFAULTS TO TERRAIN
            entity->color = COLOR_GREEN;
            entity->type = ET_TERRAIN;
            entity->flags = 0;
        }
        char32_t glyph = entity_glyph(entity);
        entity->glyph = htree_get(tile_map, glyph);
        file_view += 3;
        ++i;
    }
end:
    free(file_buf);
    fclose(fp);
    return map;
}
