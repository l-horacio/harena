#ifndef _MAP_LOADER_H_
#define _MAP_LOADER_H_

#include "size.h"

struct entity;

struct map {
    struct isize size;
    struct entity *map_entities;
};

struct htree;

struct map load_map(const char *filepath, struct htree *tile_map) ;

#endif //_MAP_LOADER_H_
