#ifndef _SIZE_H_
#define _SIZE_H_

struct size {
    float width, height;
};

struct isize {
    int width, height;
};

struct point {
    float x, y;
};

struct ipoint {
    int x, y;
};

struct rectangle {
    float x, y, w, h;
};

struct irectangle {
    float x, y, w, h;
};

struct tex_coords
{
    float tu1, tv1, tu2, tv2;
};

#define max(x, y) ((x) > (y)) ? (x) : (y)
#define min(x, y) ((x) < (y)) ? (x) : (y)
#define clamp(v, lower, upper) min(max((v), (lower)), (upper))

#endif //_SIZE_H_
