#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "size.h"
#include <uchar.h>

struct window;

enum window_events {
  WE_RESIZE = 0x01,
  WE_UPDATE = 0x02,

  // Keyboard events
  WE_KEYPRESS = 0x03,
  WE_A = 0x04,
  WE_B = 0x05,
  WE_C = 0x06,
  WE_D = 0x07,
  WE_E = 0x08,
  WE_F = 0x09,
  WE_G = 0x0A,
  WE_H = 0x0B,
  WE_I = 0x0C,
  WE_J = 0x0D,
  WE_K = 0x0E,
  WE_L = 0x0F,
  WE_M = 0x10,
  WE_N = 0x11,
  WE_O = 0x12,
  WE_P = 0x13,
  WE_Q = 0x14,
  WE_R = 0x15,
  WE_S = 0x16,
  WE_T = 0x17,
  WE_U = 0x18,
  WE_V = 0x19,
  WE_W = 0x1A,
  WE_X = 0x1B,
  WE_Y = 0x1C,
  WE_Z = 0x1D,
  WE_1 = 0x1E,
  WE_2 = 0x1F,
  WE_3 = 0x20,
  WE_4 = 0x21,
  WE_5 = 0x22,
  WE_6 = 0x23,
  WE_7 = 0x24,
  WE_8 = 0x25,
  WE_9 = 0x26,
  WE_0 = 0x27,
  WE_RETURN = 0x28,
  WE_ENTER = 0x28,
  WE_ESCAPE = 0x29,
  WE_BACKSPACE = 0x2A,
  WE_TAB = 0x2B,
  WE_SPACE = 0x2C,
  WE_MINUS = 0x2D,      /*  -  */
  WE_EQUALS = 0x2E,     /*  =  */
  WE_LBRACKET = 0x2F,   /*  [  */
  WE_RBRACKET = 0x30,   /*  ]  */
  WE_BACKSLASH = 0x31,  /*  \  */
  WE_SEMICOLON = 0x33,  /*  ;  */
  WE_APOSTROPHE = 0x34, /*  '  */
  WE_GRAVE = 0x35,      /*  `  */
  WE_COMMA = 0x36,      /*  ,  */
  WE_PERIOD = 0x37,     /*  .  */
  WE_SLASH = 0x38,      /*  /  */
  WE_F1 = 0x3A,
  WE_F2 = 0x3B,
  WE_F3 = 0x3C,
  WE_F4 = 0x3D,
  WE_F5 = 0x3E,
  WE_F6 = 0x3F,
  WE_F7 = 0x40,
  WE_F8 = 0x41,
  WE_F9 = 0x42,
  WE_F10 = 0x43,
  WE_F11 = 0x44,
  WE_F12 = 0x45,
  WE_PAUSE = 0x48, /* Pause/Break */
  WE_INSERT = 0x49,
  WE_HOME = 0x4A,
  WE_PAGEUP = 0x4B,
  WE_DELETE = 0x4C,
  WE_END = 0x4D,
  WE_PAGEDOWN = 0x4E,
  WE_RIGHT = 0x4F,       /* Right arrow */
  WE_LEFT = 0x50,        /* Left arrow */
  WE_DOWN = 0x51,        /* Down arrow */
  WE_UP = 0x52,          /* Up arrow */
  WE_KP_DIVIDE = 0x54,   /* '/' on numpad */
  WE_KP_MULTIPLY = 0x55, /* '*' on numpad */
  WE_KP_MINUS = 0x56,    /* '-' on numpad */
  WE_KP_PLUS = 0x57,     /* '+' on numpad */
  WE_KP_ENTER = 0x58,
  WE_KP_1 = 0x59,
  WE_KP_2 = 0x5A,
  WE_KP_3 = 0x5B,
  WE_KP_4 = 0x5C,
  WE_KP_5 = 0x5D,
  WE_KP_6 = 0x5E,
  WE_KP_7 = 0x5F,
  WE_KP_8 = 0x60,
  WE_KP_9 = 0x61,
  WE_KP_0 = 0x62,
  WE_KP_PERIOD = 0x63, /* '.' on numpad */
  WE_SHIFT = 0x70,
  WE_CONTROL = 0x71,
  WE_ALT = 0x72,
  WE_RELEASE = 0x100,

  // Mouse events
  WE_MOUSE = 0x7F,
  WE_MOUSE_LEFT = 0x80, /* Buttons */
  WE_MOUSE_RIGHT = 0x81,
  WE_MOUSE_MIDDLE = 0x82,
  WE_MOUSE_X1 = 0x83,
  WE_MOUSE_X2 = 0x84,
  WE_MOUSE_MOVE = 0x85,   /* Movement event */
  WE_MOUSE_SCROLL = 0x86, /* Mouse scroll event */
  WE_MOUSE_X = 0x87,      /* Cusor position in cells */
  WE_MOUSE_Y = 0x88,
  WE_MOUSE_PIXEL_X = 0x89, /* Cursor position in pixels */
  WE_MOUSE_PIXEL_Y = 0x8A,
  WE_MOUSE_WHEEL_UP = 0x8B,
  WE_MOUSE_WHEEL_DOWN = 0x8C,
};

typedef struct ipoint mouse_pos;
typedef struct isize window_size;
struct keypress_info {
    unsigned short key;
    char32_t wchar;
};

struct event {
    enum window_events code;

    union {
        mouse_pos mouse_position;
        window_size window_size;
        struct keypress_info keypress_info;
        unsigned char mouse_button;
        unsigned char _trash;
    };
};

struct state;

typedef void (*event_callback) (struct window*, struct state *, struct event);

struct window *create_window(int width, int height, event_callback callback);

int update_window(struct window* win, struct state* state);
void window_swap_buffers(struct window * win);
#endif //_WINDOW_H_
