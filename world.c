#include "world.h"
#include "entity.h"
#include "font.h"
#include "htree.h"
#include "map_loader.h"
#include "size.h"
#include "window.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <uchar.h>

static double get_miliseconds(void) {
    struct timeval s_tTimeVal;
    gettimeofday(&s_tTimeVal, 0);
    double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
    time += s_tTimeVal.tv_usec / 1000.0; // us to ms
    return time;
}

struct world create_world(int w, int h)
{
    struct htree *char_map = htree_make();
    struct world world = (struct world)
    {
        0,
        (struct state) {{0},
                        0,
                        0,
                        (struct isize) {32, 32},
                        char_map,
                        (struct size) {w, h},
                        (struct map) {{0,0},0},
                        get_miliseconds()},
    };

    world.state.player = &world.state.entities[0];

    return world;
}

static int update(float delta) {
    (void) delta;
    return 0;
}

static void render(struct world *world)
{
    glDisable(GL_SCISSOR_TEST);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);

    glColor4f(1, 1, 1, 1);

    int w = world->state.cell_size.width;
    int h = world->state.cell_size.height;

    glBegin(GL_QUADS);
    {
        unsigned y_max = min(world->state.view_size.height, world->state.map.size.height);
        unsigned x_max = min(world->state.view_size.width, world->state.map.size.width);
        for (unsigned y = 0; y < y_max; ++y) {
            for (unsigned x = 0; x < x_max; ++x) {
                int i = y * x_max + x;
                struct entity *entity = world->state.map.map_entities + i;
                entity_render(entity, x * w, y * h);
            }
        }

        for(unsigned i = 0; i < world->state.entity_count; ++i) {
            struct entity *entity = world->state.entities + i;
            entity_render(entity, w * entity->pos.x, h * entity->pos.y);
        }
    }
    glEnd();
}

void update_viewport(struct state *state, int w, int h)
{
    struct size viewport_size = (struct size){(float)w, (float)h};

    struct size stage_size = (struct size) {
        state->view_size.width * state->cell_size.width,
        state->view_size.height * state->cell_size.height,
    };

    float viewport_ratio = viewport_size.width / (float)viewport_size.height;

    // TODO: Viewport height > width
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, viewport_size.width, viewport_size.height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float sd = (stage_size.width * viewport_ratio) - stage_size.height;
    glOrtho(-sd/2, stage_size.height + sd/2, stage_size.height, 0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

int run_loop(struct world *world)
{
    double *time = &world->state.time;
    if(update_window(world->win, &world->state))
        return 1;

    double now = get_miliseconds();
    double delta = (now - (*time)) * 0.001;
    *time = now;

    if(update(delta)) {
        return 1;
    }

    render(world);
    window_swap_buffers(world->win);
    return 0;
}

struct font_char *load_font_char(struct world *world, const char32_t c)
{
    return htree_get(world->state.tile_map, c);
}

void event_handler(struct window *window, struct state *state, struct event event)
{
    (void) window;
    switch (event.code) {
    case WE_RESIZE:
        update_viewport(state, event.window_size.width, event.window_size.height);
        break;
    case WE_KEYPRESS:
        if((event.keypress_info.key & 0xff) == WE_W) {
            entity_move(&(state->entities[0]), &state->map, 0, -1);
        } else if((event.keypress_info.key & 0xff) == WE_S) {
            entity_move(&(state->entities[0]), &state->map, 0, 1);
        } else if((event.keypress_info.key & 0xff) == WE_D) {
            entity_move(&(state->entities[0]), &state->map, 1, 0);
        } else if((event.keypress_info.key & 0xff) == WE_A) {
            entity_move(&(state->entities[0]), &state->map, -1, 0);
        }
        break;
    default: break;
    }
}

void create_map(struct world *world, const char *name)
{
    world->state.map = load_map(name, world->state.tile_map);
}


void load_glyphs(struct world * world)
{
    struct font_provider *fp = load_fonts();
    set_font_size(fp, world->state.cell_size.width);

    for (char32_t i = 0; i < 128; ++i) {
        struct font_char fc;
        load_char(fp, i, &fc);
        htree_add(world->state.tile_map, i, fc);
    }

    const char32_t *glyphs = U",.`';´¸◉@░▒▓ ";
    for (unsigned i = 0; glyphs[i]; ++i) {
        char32_t c = glyphs[i];
        if(c > 127) {
            struct font_char fc;
            load_char(fp, c, &fc);
            htree_add(world->state.tile_map, c, fc);
        }
    }

    clear_fonts(fp);
}
