#ifndef _WORLD_H_
#define _WORLD_H_
#include <uchar.h>
#include "entity.h"
#include "map_loader.h"
#include "font.h"
#include "htree.h"
#include "window.h"

// TODO: implement dynamic arrays
#define MAX_ENTITIES 64

struct state {
    struct entity entities[MAX_ENTITIES];
    struct entity *player;
    unsigned entity_count;
    struct isize cell_size;
    struct htree *tile_map;
    struct size view_size;
    struct map map;
    double time;
};

struct world {
    struct window* win;
    struct state state;
};

struct world create_world(int w, int h);

int run_loop(struct world *world);

void event_handler(struct window *win, struct state *state, struct event event);

void entity_move(struct entity *entity, struct map *map, int dx, int dy);

void load_glyphs(struct world *world);
struct font_char *load_font_char(struct world *world, const char32_t c);

void create_map(struct world *world, const char *name);
#endif //_WORLD_H_
